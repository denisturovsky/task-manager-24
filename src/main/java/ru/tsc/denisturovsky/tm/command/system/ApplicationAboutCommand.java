package ru.tsc.denisturovsky.tm.command.system;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Show developer info";

    @NotNull
    public static final String FIRST_NAME = "Denis";

    @NotNull
    public static final String LAST_NAME = "Turovsky";

    @NotNull
    public static final String EMAIL = "dturovsky@t1-consulting.ru";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.format("[%s] \n", NAME.toUpperCase(Locale.ROOT));
        System.out.format("Name: %s %s \n", FIRST_NAME, LAST_NAME);
        System.out.format("E-mail: %s \n", EMAIL);
    }

}
