package ru.tsc.denisturovsky.tm.exception.system;

import ru.tsc.denisturovsky.tm.exception.field.AbstractFieldException;

public final class LoginPasswordIncorrectException extends AbstractFieldException {

    public LoginPasswordIncorrectException() {
        super("Error! Incorrect login or password entered. Please try again...");
    }

}
