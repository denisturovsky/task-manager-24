package ru.tsc.denisturovsky.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractModel {

    @NotNull
    private Boolean locked = false;

    @Nullable
    private String login;

    @Nullable
    private String passwordHash;

    @NotNull
    private String email = "";

    @NotNull
    private String firstName = "";

    @NotNull
    private String lastName = "";

    @NotNull
    private String middleName = "";

    @NotNull
    private Role role = Role.USUAL;

}
