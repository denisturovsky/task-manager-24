package ru.tsc.denisturovsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task create(@Nullable String userId,
                @Nullable String name);

    @NotNull
    Task create(@Nullable String userId,
                @Nullable String name,
                @Nullable String description);

    @NotNull
    Task create(@Nullable String userId,
                @Nullable String name,
                @Nullable String description,
                @Nullable Date dateBegin,
                @Nullable Date dateEnd);

    @Nullable
    Task changeTaskStatusById(@Nullable String userId,
                              @Nullable String id, Status status);

    @Nullable
    Task changeTaskStatusByIndex(@Nullable String userId,
                                 @Nullable Integer index,
                                 @Nullable Status status);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId,
                                  @Nullable String projectId);

    @Nullable
    Task updateOneById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable String name,
                       @Nullable String description);

    @Nullable
    Task updateOneByIndex(@Nullable String userId,
                          @Nullable Integer index,
                          @Nullable String name,
                          @Nullable String description);

}
